{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Yesod
import Data.Text

data App = App

instance Yesod App


-- domain specific language
mkYesod "App" [parseRoutes|
  /     HomeR     GET

|]

bar :: Text
bar = "123"

getHomeR :: Handler Html
getHomeR = defaultLayout $ do
  setTitle "My Yesod"
  [whamlet|
    <h1> Hello, World!
    ^{foo}
  |]
  toWidgetBody [hamlet|
    <h2 #abc> Only hamlet
    <h3> #{bar}
    @?{(HomeR, [("test", "123")])}

    $if True
      <div>
    $elseif True
      <div>
    $else
      <div>
  |]
  foo
  addScriptRemote ""
  addStylesheetRemote ""

  --addScript HomeR
  --addStylesheet Home R


foo = [whamlet|
        <h2> Foo
|]

main :: IO ()
main = warp 8080 App