module Aula02 where

-- TIPOS

--data Animal = Cachorro | Gato | Rato

-- data Bool = True | False

{- barulho :: Animal -> String
barulho Cachorro = "au au"
barulho Gato = "miau"
barulho Rato = "squik" -}

-- PATTERN MATCHING

foo :: (Int, Char) -> String --tuple
--foo x = replicate (fst x) (snd x)
foo (i, 'a') = replicate (2 * i) 'a' -- ordem importa para o pattern matching
foo (i, c) = replicate i c --ghci foo (10, 'c')

foo' :: Int -> Char -> String
foo' i 'a' = replicate (2 * i) 'a'
foo' i c = replicate i c

bar :: (Char, Char) -> Char
bar (x, 'z')  = x
bar ('z', y)  = y
bar ('c', _)  = '1' -- _ usado para ignorar o valor
bar _         = '2'



head' :: [a] -> a
head' (x : xs) = x -- xs representa o resto a lista
-- head' (x : _) = x -- outra opção


foo'' :: [Int] -> Int
foo'' [] = 0
foo'' (x:y:z:[]) = x * y * z
foo'' (x:_:y:z:_) = x * y + z




data Animal = Cachorro String
              | Gato String Int

info :: Animal -> String
info (Cachorro nome) = "O cachorro se chama " ++ nome
info (Gato nome vidas) =
    "O gato se chama "++ nome ++
    " e tem " ++ (show vidas) ++ " vidas"


nome :: Animal -> String
nome (Cachorro nome) = nome
nome (Gato nome _) = nome



data Roupa = Blusa { marca :: String }
            | Short { marca :: String, preco :: Int } -- função parcial, pois é tipo Roupa porém tem o record syntax preço

info'' :: Roupa -> String
-- info'' (Short x _) = "A marca do short eh " ++ x
info'' (Short { marca = x }) = "A marca do short eh " ++ x


dobro :: Int -> Int
dobro x = x * 2



-- LAMBDAS
-- \p1 p2 ... pn -> expressão resultado


bar'' :: (Int -> Int) -> Int
bar'' f = f 123


-- CURRY/ UNCURRY

mult3 :: Int -> Int -> Int -> Int
mult3 a b c = a * b * c


asd :: Int -> Int
-- asd x = mult3 2 3 x
asd = mult3 2 3 -- curry

-- curry :: ((a, b) -> c) -> a -> b -> c
-- uncurry :: (a -> b -> c) -> (a, b) -> c


-- map :: (a -> b) -> [a] -> [b]



-- foldl :: (b -> a -> b) -> b -> [a] -> b

-- ex1:
-- foldl (+) 0 [1 .. 5]

-- foldl (+) 0 [1,2,3,4,5]
-- foldl (+) 1 [2,3,4,5]
-- foldl (+) 3 [3,4,5]
-- foldl (+) 6 [4,5]
-- foldl (+) 10 [5]
-- foldl (+) 15 [0]
-- 15

-- ex2:
-- foldl (\xs c -> c:xs) [] "haskell"
-- (\xs c -> c:xs) :: [a] -> a -> [a] ou flip

-- foldl (\xs c -> c:xs) "h" "askell"
-- foldl (\xs c -> c:xs) "ah" "skell"
-- foldl (\xs c -> c:xs) "sah" "kell"
-- foldl (\xs c -> c:xs) "ksah" "ell"
-- foldl (\xs c -> c:xs) "eksah" "ll"
-- foldl (\xs c -> c:xs) "leksah" "l"
-- foldl (\xs c -> c:xs) "lleksah" ""
-- "lleksah"

-- $ substitui os parênteses (altera ordem de execução)

-- take :: Int -> [a] -> [a]
-- take 2 [1..10]
-- [1,2]


-- usando $

-- foo''' text = reverse (tail (take 3 (reverse text)))
foo''' text = reverse  $ tail  $ take 3 $ reverse text


-- Definindo um operador
(++--++) :: Int -> Int -> Int
x ++--++ y = x + y
infixr 5 ++--++


-- Guards
qwerty :: Int -> String
qwerty x  | x < 10 = "pequeno"
          | x < 20 = "medio"
          | x < 30 = "alto"
          | otherwise ="qwerty"