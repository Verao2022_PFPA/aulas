{-# LANGUAGE FlexibleInstances #-}
module Aula03 where
import Data.Monoid

-- recursão
-- condição de parada
-- passo da recursão

reverse' :: [a] -> [a]
reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x]

-- polimorfismos: ad-hoc (coersão e sobrecarga) & universal (subtipo e paramétrico)
-- polimorfismo paramétrico (mesma estrtura de função e diferentes tipos)

data Foo a = Bar a Int a deriving Show-- a é type parameter

--List
-- data [] a = []
--           | a : [a]

-- Tuple
-- data (,) a b = (a,b)

-- Àrvore binária

data Tree a = Nil
            | Node { left :: Tree a, value :: a, right :: Tree a }

{-                d
                 /\
                a  b -}

-- :r Nil
-- Nil :: Tree a (poliformismo!)


inOrder :: Tree a -> [a]
inOrder Nil = []
inOrder (Node l v r) = (inOrder l) ++ [v] ++ (inOrder r)


-- Sobrecarga

-- Show
-- show :: Show a => a -> String
data Foo' = Foo' Int deriving (Show, Eq)


data Foo'' a = Foo'' a

instance Show (Foo'' Integer) where
  show (Foo'' x) = ">>>" ++ show x ++ "<<<"

instance Show (Foo'' [Char]) where
  show (Foo'' x) = "<<<" ++ show x ++ ">>>"

instance Show (Foo'' Char) where
  show (Foo'' x) = "!!!" ++ show x ++ "!!!"


-- Type Class

data Alfabeto = A | B | C | D | E
  deriving (Show, Enum, Eq, Ord, Bounded)

-- Monoid (type class)

-- associatividade
-- (a <> b) <> c = a <> (b < c>)

-- elemento neutro
-- e <> x = x <> e = x

{-

class Monoid a where
  mappend :: a -> a -> a --associatividade
  mempty :: a -- el neutro

(<>) = mappend


instance Num a => Monoid (Sum a) where
  mappend = (+)
  mempty = 0

instance Num a => Monoid (Product a) where
  mappend = (*)
  mempty = 1

  -}


-- Teoria de Categoria
-- categorias são monóides

{-

class Category cat where
  mn :: cat a a -- morfirsmo neutro
  comp :: cat b c -> cat a b -> a c -- composição

class Category (->) where -- Hask (->) where
  mn:: a -> a
  mn = id
  mn :: cat a a -- morfirsmo neutro
  comp :: (b -> c) -> (a -> b) -> (a -> c)
  comp :: ((->) b c) -> ((->) a b) -> ((->) a c)
  comp :: cat b c -> cat a b -> a c -- composição
  comp = (.)

-}




