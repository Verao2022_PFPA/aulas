module ListaEx where

tria :: Int -> Int -> Int -> Bool
tria x y z  = (x < y + z) && (y < x + z) && (z < x + y)

pot :: Int -> Int -> Int
pot x y   | (y == 0) = 1
          | (y == 1) = x
pot x y = x * pot x (y - 1)

--elem :: [x] -> y -> Bool
--elem [x] y = y


-- data Cor = Cor { red :: Int, green :: Int, blue :: Int}

data Cor = Cor Int Int Int

-- somaCor :: Cor -> Cor -> Cor
-- somaCor c1 c2 = c1 + c2

data Cofre a = Cofre [a] deriving Show

instance Functor Cofre where
  fmap f (Cofre (x:xs)) = Cofre (f x : fmap f xs)

-- instance Applicative Cofre where
  -- pure x = [x]

  -- fs <*> xs = Cofre ([f x | f <- fs, x <- xs])


data Automovel = Carro | Moto

data Veiculo = Veiculo { nomeAuto :: Automovel, placa :: String }

class EhCarro a where
  ehcarro :: a -> Bool

-- instance EhCarro Veiculo where
  -- ehcarro :: Veiculo -> Bool


data JoKenPo = Pedra | Papel | Tesoura deriving Show

data Resultado = J1 | J2 | Emp deriving Show

jogar :: JoKenPo -> JoKenPo -> Resultado
jogar Pedra Pedra     = Emp
jogar Pedra Papel     = J2
jogar Pedra Tesoura   = J1
jogar Papel Pedra     = J1
jogar Papel Papel     = Emp
jogar Papel Tesoura   = J2
jogar Tesoura Pedra   = J2
jogar Tesoura Papel   = J1
jogar Tesoura Tesoura = Emp



recursiva :: Int -> [Int] -> Bool
recursiva y (x:xs)  | y == x            = True
                    | y `recursiva` xs  = True
recursiva y []                          = False