{-# LANGUAGE ParallelListComp #-}
module Modulo where

-- {-# LANGUAGE ParallelListComp #-} pragma/ extensão de linguagem

  {-

  comentário de bloco

  -}

-- funções (camelCase)
-- nomeDaFuncao p1 p2 p3 ... pn = expressão

-- exemplo c
-- int f(int x)
-- {
--    return x * 2
-- }

-- exemplo haskell

f :: Int -> Int -- assinatura da função / parametro -> retorno
f x = x * 2

-- exemplo 2 c
-- int f(int x, int Y)
-- {
--    return x * y
-- }

f' :: Int -> Int -> Int
f' x y = x * y

-- operadores são funções
-- 2 + 3
-- 5

-- (+) 2 3
-- 5


x :: Int -- função sem parâmetro
x = 5

-- sinonimos de tipos
-- type String = [Char]

-- Strings
text :: String
text = "abc"

-- Listas

-- Funções parciais
-- reverse inverte a lista
-- funções head, tail, init, last outras funções para listas
-- last é a composição das funções reverse e head
-- last = head . reverse
-- !!

-- problema: listas vazias!

-- Range
-- [1 .. 10]
-- [1, 3 .. 10]

-- List Comprehensions
-- [expressão x | "iterações", filtros/predicados]