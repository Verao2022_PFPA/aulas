{-# LANGUAGE NoImplicitPrelude #-}
module Aula04 where

import Prelude (Show, ($), (*), show)

-- F
-- a :: A -- F(a) :: B

-- f :: x -> y     -- x,y :: A
-- F(f) :: F(x) -> F(y)     --F(x), F(y) :: B

-- identidade
-- F(id(x)) = id (F(x))    -- x :: A
-- composição
-- F(f . g) = F(f) . F(g) -- f, g :: A |


class Functor f where
  fmap :: (a -> b) -> f a -> f b

-- fmap id = id
-- fmap (f . g) == fmap f . fmap g

instance Functor [] where
  -- fmap :: (a -> b) -> [a] -> [b]
  fmap _ []       = []
  fmap f (x:xs) = f x : fmap f xs
  -- ou
  -- fmap f xs = [f x | x <- xs]


data Maybe a = Just a | Nothing deriving Show

head :: [a] -> a
head (x:_) = x

safeHead :: [a] -> Maybe a
safeHead (x:_)  = Just x
safeHead []     = Nothing


instance Functor Maybe where
  --  fmap :: (a -> b) -> f a -> f b
      fmap f (Just x) = Just $ f x
      fmap _ Nothing = Nothing



data Caixa a b c = Caixa a b c deriving Show

instance Functor (Caixa e r) where
  --  fmap :: (a -> b) -> Caixa a -> Caixa b
      fmap f (Caixa x y z) = Caixa x y (f z)


-- funtor aplicativo (de uma caixinha para outra caixinha)
-- * -> *
class Functor f => Applicative f where -- com restrição
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b

instance Applicative Maybe where
-- pure :: a -> Maybe a
  pure x = Just x

-- (<*>) :: Maybe (a -> b) -> Maybe a -> Maybe b
  (Just f) <*> (Just x) = Just $ f x
  _ <*> _ = Nothing


instance Applicative [] where
-- pure :: a -> [a]
  pure x = [x]

-- (<*>) :: [a -> b] -> [a] -> [b]
  fs <*> xs = [f x | f <- fs, x <- xs]



-- MONADS

-- :: (Functor f, Functor g) => f a -> g a
-- :: Tree a -> a

data Identity a = Identity a deriving Show


instance Functor Identity where
--  fmap :: (a -> b) -> Identity a -> Identity b
    fmap f (Identity x) = Identity $ f x

-- pure :: Applicative f => a -> f a
-- pure :: Applicative f => Identity a -> f a


-- uma monad é apenas um monoid dentro da categoria dos endofuntores

-- return :: a -> m a
-- join :: m (m a) -> m a

-- * -> *
class Applicative m => Monad m where
  return :: a -> m a
  (>>=) :: m a -> (a -> m b ) -> m b  -- bind

-- m a -> (a -> m b) -> m b
-- a -> (a -> b) -> b


instance Monad Maybe where
--  return :: a -> Maybe a
    return x = Just x
    -- ou
    -- return = Just
    -- ou
    -- return = pure

    -- (>>=) :: Maybe a -> (a -> Maybe b ) -> Maybe b
    (Just x) >>= f = f x
    _ >>= _ = Nothing














-- inverte $

-- (2*) $ head $ tail $ [1,2,3]
-- (|>) = flip ($)
-- [1,2,3] |> tail |> head |> (2*)







-- sobre kinds

-- * -> * -> *
-- data Either (a :: *) (b :: *)

-- (* -> *) -> * -> *
-- data Foo (f :: * -> *) (a :: *) = Bar Int a (f a) (f Bool)












